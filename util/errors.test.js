import { describe, it, expect } from 'vitest';
import { HttpError, ValidationError } from './errors';

describe('class HttpError', () => {
    it('should contain the provided status code, message and body', () => {
        const statusCode = 200;
        const message = 'HTTP Test';
        const data = { key: 'test' };

        const res = new HttpError(statusCode, message, data);

        expect(res.statusCode).toBe(statusCode);
        expect(res.message).toBe(message);
        expect(res.data).toBe(data);
    });

    it('should contain undefined as data if not data is provided', () => {
        const res = new HttpError();

        expect(res.statusCode).toBeUndefined();
        expect(res.message).toBeUndefined();
        expect(res.data).toBeUndefined();
    });
});

describe('class ValidationError', () => {
    it('should contain the provided message', () => {
        const message = 'test';
        
        const res = new ValidationError(message);

        expect(res.message).toBe(message);
    });
});