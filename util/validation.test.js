import { it, expect } from "vitest";
import { validateNotEmpty } from './validation'; 

it('should throw an error if empty string is passed', () => {
    const input = '';

    const res = () => validateNotEmpty(input);

    expect(res).toThrow();
});

it('should throw an error if string with whitespace is passed', () => {
    const input = '         ';
    const errMsg = 'error message';

    const res = () => validateNotEmpty(input, errMsg);

    expect(res).toThrow(errMsg);
});